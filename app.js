const http = require('http');
const nodemailer = require('nodemailer');

const HOSTNAME = 'localhost'
const PORT = 3000;  

const EMAIL_SENDER = '';    
const SENDER_PASSWORD = '';  
const EMAIL_RECIEVER = '';  


const server = http.createServer((req, res) => {
    if (req.method == 'POST') {
        let body = '';
        req.on('data', chunk => {
            body += chunk.toString(); // convert Buffer to string
        });

        req.on('end', () => {
            body = JSON.parse(body);

            if(!body.name) return res.end('missing name!');
            if(!body.phone) return res.end('missing phone!');
            if (!validateEmail(body.email)) return res.end("invalid email address!");

     
            const transporter = nodemailer.createTransport({
                service: 'gmail',
                auth: {
                    user: EMAIL_SENDER, 
                    pass: SENDER_PASSWORD
                }
            });

            const mailOptions = {
                from: EMAIL_SENDER,
                to: EMAIL_RECIEVER,
                subject: 'Заявка с blackbird',
                text: 
                    `
                        Имя — ${body.name}
                        Почта — ${body.email}
                        Телефон — ${body.phone}
                        Сообщение — ${body.message}
                    `
               
            };

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log(error);
                    res.end(error.message);
                } else {
                    res.end('Email sent: ' + info.response);
                }
            });
      
        });
    }
});

server.listen(PORT, HOSTNAME, () => {
    console.log(`Server running at http://${HOSTNAME}:${PORT}/`);
});



function validateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) 
        return true;
    return false;
}